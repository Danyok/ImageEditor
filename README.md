# Full featured image editor for Aurora OS

Full featured image editor for Aurora OS

## Command line arguments

Application allows to specify the path to the image that should be edited. Specify it via a command line argument `--image`.

## Features

The application has been tested for the following features:

* The application allows to select the image from the images available on the device.
* The application allows to view the image that is passed via `--image` argument on the command line.
* After the editor launch the pencil tool is automatically selected.
* The application allows to modify the width of the pencil and the color of the pencil.
* The application shows the color and the width that were used by the pen tool previously.
* The application allows to zoom in and zoom out using the pinch gesture.
* The application allows to drag the image using two-finger gesture.
* The application allows to rollback the last drawing of the pencil tool.
* The application allows to set up the image brightness.
* The application allows to rollback the brightness changes.
* The application allows to set up the image contrast.
* The application allows to rollback the contrast changes.
* The application allows to crop the images using the "Original", "Custom", "Square", 4:3 and 3:4 rations.
* The application allows to rollback the cropping selection.
* The application allows to rotate the image to the 90 degrees clockwise and counterclockwise.
* The application allows to discard all the changes by discarding the dialog.
* The application allows to save all changes into a file that has been edited.
* The application allows to save all changes into a new file.
* The application allows to create a new empty image by setting the image width, height, format, quality, background color and path to save.

## Terms of Use and Participation

The source code of the project is provided under [the license](LICENSE.BSD-3-Clause.md),
that allows it to be used in third-party applications.


The [contributor agreement](CONTRIBUTING.md) documents the rights granted by contributors
to the Open Mobile Platform.

For information about contributors see [AUTHORS](AUTHORS.md).

[Code of conduct](CODE_OF_CONDUCT.md) is a current set of rules of the Open Mobile
Platform which informs you how we expect the members of the community will interact
while contributing and communicating.

## Project Structure

The project has a common structure of an application based on C++ and QML for Aurora OS.

* **[app](app)** subproject contains the application source code:
  * **[app.pro](app/app.pro)** file 
    describes the app subproject structure for the qmake build system.
  * **[icons](app/icons)** directory contains application icons for different screen resolutions.    
  * **[qml](app/qml)** directory contains the QML source code and the UI resources.
    * **[components](app/qml/components)** directory contains the custom UI components.
    * **[cover](app/qml/cover)** directory contains the application cover implementations.
    * **[dialogs](app/qml/dialogs)** directory contains the application dialogs.
    * **[icons](app/qml/icons)** directory contains the custom UI icons.
    * **[pages](app/qml/pages)** directory contains the application pages.
    * **[ImageEditor.qml](app/qml/ImageEditor.qml)** file
      provides the application window implementation.
  * **[src](app/src)** directory contains the C++ source code.
    * **[main.cpp](app/src/main.cpp)** file is the application entry point.
  * **[translations](app/translations)** directory contains the UI translation files.
  * **[ru.auroraos.ImageEditor.desktop](ru.auroraos.ImageEditor.desktop)** file
    defines the display and parameters for launching the application.
* **[ru.auroraos.ImageEditor.pro](ru.auroraos.ImageEditor.pro)** file
  describes the project structure for the qmake build system.
* **[rpm](rpm)** directory contains the rpm-package build settings.
  * **[ru.auroraos.ImageEditor.spec](rpm/ru.auroraos.ImageEditor.spec)** file is used by rpmbuild tool.
  
## Compatibility
  
The project is compatible with all the supported versions of the Aurora OS.

## Project Building

The project is built in the usual way using the Aurora SDK.

## Screenshots

![screenshots](screenshots/screenshots.png)

## This document in Russian / Перевод этого документа на русский язык

- [README.ru.md](README.ru.md)
