# Authors

* Andrey Vasiliev
  * Developer, 2023
* Ivan Shchitov
  * Developer, 2023
* Nikita Medvedev
  * Developer, 2023
* Evgeniy Samohin, <e.samohin@omp.ru>
  * Reviewer, 2023
* Konstantin Zvyagin, <k.zvyagin@omp.ru>
  * Reviewer, 2023
* Andrey Tretyakov, <a.tretyakov@omp.ru>
  * Reviewer, 2023
* Vladislav Larionov
  * Reviewer, 2023
