# SPDX-FileCopyrightText: 2023 Open Mobile Platform LLC <community@omp.ru>
# SPDX-License-Identifier: BSD-3-Clause

TARGET = ru.auroraos.ImageEditor

QT += dbus

CONFIG += \
    auroraapp

PKGCONFIG += \

DEFINES += \
    DOCUMENTS_LOCATION=\\\"/Documents/ru.auroraos.ImageEditor\\\" \

SOURCES += \
    src/createemptyimagedialogcontroller.cpp \
    src/cropselectioncalculator.cpp \
    src/imageeditordialogcontroller.cpp \
    src/main.cpp \
    src/pensettingscontroller.cpp \
    src/settings.cpp \
    src/eventlogger.cpp \

HEADERS += \
    src/createemptyimagedialogcontroller.h \
    src/cropselectioncalculator.h \
    src/imageeditordialogcontroller.h \
    src/pensettingscontroller.h \
    src/settings.h \
    src/eventlogger.h \

AURORAAPP_ICONS = 86x86 108x108 128x128 172x172

CONFIG += auroraapp_i18n

TRANSLATIONS += \
    translations/ru.auroraos.ImageEditor.ts \
    translations/ru.auroraos.ImageEditor-ru.ts \
