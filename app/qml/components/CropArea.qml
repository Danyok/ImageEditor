// SPDX-FileCopyrightText: 2023 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

import QtQuick 2.0
import ru.auroraos.ImageEditor 1.0

MouseArea {
    id: root

    objectName: "cropArea"

    property alias shadow: topShadow
    property alias viewfinder: viewfinder

    readonly property bool cropInProgress: pressed

    property real interactiveBorderWidth: 1
    property real minLeft: 0
    property real maxRight: width
    property real minTop: 0
    property real maxBottom: height
    property real minWidth: interactiveBorderWidth
    property real minHeight: interactiveBorderWidth

    property int _cropFactorX: 0
    property int _cropFactorY: 0
    property real _mouseX: 0
    property real _mouseY: 0
    property real _minMouseX: 0
    property real _minMouseY: 0
    property real _maxMouseX: 0
    property real _maxMouseY: 0
    property real _viewfinderX: 0
    property real _viewfinderY: 0
    property real _viewfinderWidth: 0
    property real _viewfinderHeight: 0
    property bool canUndo: false

    property int cropFormat: Settings.Original
    property var targetImage
    property real viewportScale

    signal cropStarted
    signal cropUpdated
    signal cropFinished

    function _setViewfinderRect(cropRect) {
        viewfinder.x = cropRect.x;
        viewfinder.y = cropRect.y;
        viewfinder.width = cropRect.width;
        viewfinder.height = cropRect.height;
    }

    function _initViewfinderRect() {
        var newCropRect = cropCalculator.createCropAreaRect(Qt.rect(viewfinder.x, viewfinder.y, viewfinder.width, viewfinder.height));
        _setViewfinderRect(newCropRect);
    }

    CropSelectionCalculator {
        id: cropCalculator

        cropFormat: root.cropFormat
        onCropFormatChanged: {
            if (cropFormat === Settings.Original) {
                var ratio = targetImage.width / targetImage.height;
                viewfinder.height = viewfinder.width / ratio;
            }
            _initViewfinderRect();
        }
    }

    onCanUndoChanged: {
        if (!canUndo)
            _initViewfinderRect();
    }

    onVisibleChanged: {
        if (visible)
            _initViewfinderRect();
    }

    onPressed: {
        _mouseX = mouse.x;
        _viewfinderX = viewfinder.x;
        _viewfinderWidth = viewfinder.width;
        var mouseShiftX = _mouseX - _viewfinderX;
        if (Math.abs(mouseShiftX) <= interactiveBorderWidth) {
            _cropFactorX = -1;
            _minMouseX = mouseShiftX + minLeft;
            _maxMouseX = _mouseX + _viewfinderWidth - minWidth;
        } else if (Math.abs(mouseShiftX -= _viewfinderWidth) <= interactiveBorderWidth) {
            _cropFactorX = 1;
            _minMouseX = _mouseX - _viewfinderWidth + minWidth;
            _maxMouseX = mouseShiftX + maxRight;
        } else {
            _cropFactorX = 0;
        }
        _mouseY = mouse.y;
        _viewfinderY = viewfinder.y;
        _viewfinderHeight = viewfinder.height;
        var mouseShiftY = _mouseY - _viewfinderY;
        if (Math.abs(mouseShiftY) <= interactiveBorderWidth) {
            _cropFactorY = -1;
            _minMouseY = mouseShiftY + minTop;
            _maxMouseY = _mouseY + _viewfinderHeight - minHeight;
        } else if (Math.abs(mouseShiftY -= _viewfinderHeight) <= interactiveBorderWidth) {
            _cropFactorY = 1;
            _minMouseY = _mouseY - _viewfinderHeight + minHeight;
            _maxMouseY = mouseShiftY + maxBottom;
        } else {
            _cropFactorY = 0;
        }
        if (_cropFactorX || _cropFactorY)
            cropStarted();
        else
            mouse.accepted = false;
    }

    onReleased: {
        if (_cropFactorX || _cropFactorY) {
            _cropFactorX = 0;
            _cropFactorY = 0;
            cropFinished();
            canUndo = true;
        }
    }

    onPositionChanged: {
        var tarRect;
        var vfRect = Qt.rect(viewfinder.x, viewfinder.y, viewfinder.width, viewfinder.height);
        var divx = targetImage.height / 2 - targetImage.width / 2;
        var divy = targetImage.width / 2 - targetImage.height / 2;
        if ((targetImage.rotation / 90) % 2 === 0) {
            tarRect = Qt.rect(targetImage.x * viewportScale + vfRect.x, targetImage.y * viewportScale + vfRect.y, targetImage.width * viewportScale, targetImage.height * viewportScale);
        } else {
            tarRect = Qt.rect((targetImage.x - divx) * viewportScale + vfRect.x, (targetImage.y - divy) * viewportScale + vfRect.y, targetImage.height * viewportScale, targetImage.width * viewportScale);
        }
        var newCropRect = cropCalculator.calculateNewCropAreaRect(Qt.point(mouseX, mouseY), vfRect, tarRect);
        _setViewfinderRect(newCropRect);
        if (_cropFactorX || _cropFactorY)
            cropUpdated();
        canUndo = true;
    }

    Rectangle {
        id: viewfinder

        objectName: "viewfinder"
        color: "transparent"
        z: 2
        x: root.minLeft
        y: root.minTop
        width: root.maxRight - x
        height: root.maxBottom - y
    }

    Rectangle {
        id: topShadow

        objectName: "topShadow"
        color: "black"
        z: 1
        anchors {
            left: parent.left
            right: parent.right
            top: parent.top
            bottom: viewfinder.top
        }
    }

    Rectangle {
        id: bottomShadow

        objectName: "bottomShadow"
        color: parent.shadow.color
        opacity: parent.shadow.opacity
        z: parent.shadow.z
        anchors {
            left: parent.left
            right: parent.right
            top: viewfinder.bottom
            bottom: parent.bottom
        }
    }

    Rectangle {
        id: leftShadow

        objectName: "leftShadow"
        color: parent.shadow.color
        opacity: parent.shadow.opacity
        z: parent.shadow.z
        anchors {
            left: parent.left
            right: viewfinder.left
            top: topShadow.bottom
            bottom: bottomShadow.top
        }
    }

    Rectangle {
        id: rightShadow

        objectName: "rightShadow"
        color: parent.shadow.color
        opacity: parent.shadow.opacity
        z: parent.shadow.z
        anchors {
            left: viewfinder.right
            right: parent.right
            top: topShadow.bottom
            bottom: bottomShadow.top
        }
    }
}
