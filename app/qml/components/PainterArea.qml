// SPDX-FileCopyrightText: 2023 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

import QtQuick 2.0

MouseArea {
    id: root

    objectName: "painterArea"

    readonly property bool painting: pressed

    property bool canUndo: false
    property string lineColor: "red"
    property real lineWidth: 1

    function undo() {
        if (canvas.lines.length < 1)
            return;
        canvas.lines.pop();
        canUndo = canvas.lines.length > 0;
        redraw();
    }

    function draw(line) {
        canvas.lines.push(line);
        redraw();
    }

    function moveGraphics(other, widthScale) {
        canvas.lines.forEach(function (line) {
                other.draw({
                        "color": line.color,
                        "width": line.width * widthScale,
                        "points": line.points.map(function (point) {
                                var otherPoint = mapToItem(other, point.x, point.y);
                                return {
                                    "x": otherPoint.x,
                                    "y": otherPoint.y
                                };
                            })
                    });
            });
        other.canUndo = true;
        canUndo = false;
        canvas.lines = [];
        redraw();
    }

    function redraw() {
        canvas.fullRedraw = true;
        canvas.requestPaint();
    }

    onPressed: {
        canvas.lastPaintedPoint = 0;
        canvas.lines.push({
                "color": root.lineColor,
                "width": root.lineWidth,
                "points": [{
                        "x": mouse.x,
                        "y": mouse.y
                    }]
            });
    }

    onPositionChanged: {
        if (!pressed)
            return;
        canvas.lines[canvas.lines.length - 1].points.push({
                "x": mouse.x,
                "y": mouse.y
            });
        canvas.requestPaint();
    }

    onReleased: {
        if (canvas.lines[canvas.lines.length - 1].points.length < 2) {
            canvas.lines.pop();
            redraw();
        } else {
            canUndo = true;
        }
    }

    Canvas {
        id: canvas

        objectName: "canvas"
        anchors.fill: parent

        property var lines: []
        property bool fullRedraw: false
        property int lastPaintedPoint: 0

        function drawLinePoints(line, startPoint) {
            startPoint = startPoint || 0;
            context.lineWidth = line.width;
            context.beginPath();
            var point = line.points[startPoint];
            context.moveTo(point.x, point.y);
            while (++startPoint < line.points.length) {
                point = line.points[startPoint];
                context.lineTo(point.x, point.y);
            }
            context.strokeStyle = line.color;
            context.stroke();
        }

        onPaint: {
            if (!getContext("2d"))
                return;
            context.lineCap = "round";
            context.lineJoin = "round";
            if (fullRedraw) {
                fullRedraw = false;
                context.clearRect(region.x, region.y, region.width, region.height);
                lines.forEach(function (line) {
                        drawLinePoints(line, 0);
                    });
            } else {
                var line = lines[lines.length - 1];
                if (!line)
                    return;
                drawLinePoints(line, lastPaintedPoint);
                lastPaintedPoint = line.points.length - 1;
            }
        }
    }
}
