// SPDX-FileCopyrightText: 2023 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

import QtQuick 2.0
import Sailfish.Silica 1.0

IconButton {
    icon.sourceSize: Qt.size(Theme.iconSizeSmallPlus, Theme.iconSizeSmallPlus)
    icon.source: Theme.colorScheme === Theme.DarkOnLight ? Qt.resolvedUrl("../icons/dark/icon-s-reset-setting.svg") : Qt.resolvedUrl("../icons/light/icon-s-reset-setting.svg")
}
