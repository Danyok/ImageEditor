// SPDX-FileCopyrightText: 2023 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

import QtQuick 2.0
import Sailfish.Silica 1.0

Dialog {
    id: saveImageDialog

    property string imagePath
    property var image
    property bool shouldCreateNewImage
    property string folderPathForNewFile

    acceptDestination: Qt.resolvedUrl("../pages/MainPage.qml")
    acceptDestinationAction: PageStackAction.Replace
    acceptDestinationReplaceTarget: null

    onAccepted: {
        if (shouldCreateNewImage || copySwitch.checked) {
            var dateMark = new Date().toLocaleString(Qt.locale(), "yyyyMMdd_hhmmss");
            var imageCopyName = "IMG_%1.%2".arg(dateMark).arg(imagePath.slice(imagePath.lastIndexOf(".") + 1));
            var imagePathFolder = imagePath.slice(0, imagePath.lastIndexOf("/"));
            var imageFolder = shouldCreateNewImage ? folderPathForNewFile : imagePathFolder;
            image.saveToFile("%1/%2".arg(imageFolder).arg(imageCopyName));
        } else {
            image.saveToFile(imagePath);
        }
    }

    objectName: "saveImageDialog"

    Column {
        anchors.fill: parent

        DialogHeader {
            acceptText: qsTr("Save")
            cancelText: qsTr("Cancel")
        }

        Label {
            text: qsTr("Do you want to save new image?")
            font.pixelSize: Theme.fontSizeLarge
            wrapMode: Text.WrapAtWordBoundaryOrAnywhere
            visible: shouldCreateNewImage

            anchors {
                left: parent.left
                right: parent.right
                leftMargin: Theme.horizontalPageMargin
                rightMargin: Theme.horizontalPageMargin
            }
        }

        Column {
            visible: !shouldCreateNewImage
            anchors {
                left: parent.left
                right: parent.right
                leftMargin: Theme.horizontalPageMargin
                rightMargin: Theme.horizontalPageMargin
            }

            Label {
                text: qsTr("Saving method:")
                font.pixelSize: Theme.fontSizeLarge
                wrapMode: Text.WrapAtWordBoundaryOrAnywhere

                anchors {
                    left: parent.left
                    right: parent.right
                    leftMargin: Theme.horizontalPageMargin
                    rightMargin: Theme.horizontalPageMargin
                }
            }

            TextSwitch {
                id: replaceSwitch

                onClicked: {
                    if (!checked) {
                        checked = true;
                        copySwitch.checked = false;
                    }
                }

                automaticCheck: false
                text: qsTr("Replace")
                description: qsTr("Replace source file with edited image")
                checked: true

                anchors {
                    left: parent.left
                    right: parent.right
                }
            }

            TextSwitch {
                id: copySwitch

                onClicked: {
                    if (!checked) {
                        checked = true;
                        replaceSwitch.checked = false;
                    }
                }

                automaticCheck: false
                text: qsTr("Copy")
                description: qsTr("Create edited copy of a source file")

                anchors {
                    left: parent.left
                    right: parent.right
                }
            }
        }
    }
}
