// SPDX-FileCopyrightText: 2023 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

import QtQuick 2.0
import Sailfish.Silica 1.0
import ru.auroraos.ImageEditor 1.0

Dialog {
    id: root

    // 8K resoluiton with ratio 4:3
    property int maxWidth: 8192
    property int maxHeight: 6144

    property alias imageWidth: widthTextField.text
    property alias imageHeight: heightTextField.text
    property alias imageFormat: imageFormatComboBox.value
    property alias imageQuality: qualitySlider.value

    property color chosenColor: "#ffffff"
    property string chosenFolderPath: StandardPaths.pictures
    property string cacheFolderPath: StandardPaths.cache

    acceptDestination: Qt.resolvedUrl("ImageEditorDialog.qml")
    acceptDestinationAction: PageStackAction.Replace

    canAccept: widthTextField.acceptableInput && heightTextField.acceptableInput && chosenColor.length !== 0

    onAccepted: {
        if (acceptDestination) {
            var imagePath = controller.createAndSaveImage(imageWidth, imageHeight, imageFormat, chosenColor, imageQuality, cacheFolderPath);
            acceptDestinationInstance.imagePath = imagePath;
            acceptDestinationInstance.shouldCreateNewImage = true;
            acceptDestinationInstance.folderPathForNewFile = chosenFolderPath.replace("file://", "");
        }
    }

    CreateEmptyImageDialogController {
        id: controller
    }

    SilicaFlickable {
        anchors.fill: parent

        DialogHeader {
            id: dialogHeader

            objectName: "dialogHeader"
            title: qsTr("Create an image")
            acceptText: qsTr("Create")
            cancelText: qsTr("Cancel")
        }

        Column {
            anchors {
                top: dialogHeader.bottom
                left: parent.left
                right: parent.right
            }

            TextField {
                id: widthTextField

                label: qsTr("Width, min: 200, max: %1").arg(maxWidth)
                placeholderText: qsTr("Width")
                inputMethodHints: Qt.ImhDigitsOnly
                validator: IntValidator {
                    bottom: 200
                    top: maxWidth
                }
                text: Screen.width

                EnterKey.iconSource: "image://theme/icon-m-enter-next"
                EnterKey.enabled: text.length > 0
                EnterKey.onClicked: heightTextField.focus = true
            }

            TextField {
                id: heightTextField

                label: qsTr("Height, min: 200, max: %1").arg(maxHeight)
                placeholderText: qsTr("Height")
                inputMethodHints: Qt.ImhDigitsOnly
                validator: IntValidator {
                    bottom: 200
                    top: maxHeight
                }
                text: Screen.height

                EnterKey.enabled: text.length > 0
            }

            ComboBox {
                id: imageFormatComboBox

                label: qsTr("Image format")
                menu: ContextMenu {
                    MenuItem {
                        text: "bmp"
                    }
                    MenuItem {
                        text: "jpg"
                    }
                    MenuItem {
                        text: "png"
                    }
                }
            }

            Slider {
                id: qualitySlider

                anchors {
                    left: parent.left
                    right: parent.right
                }
                visible: imageFormatComboBox.value.indexOf("bmp") !== 0
                label: qsTr("Quality")
                minimumValue: 1
                maximumValue: 100
                stepSize: 1
                value: 100
                valueText: value + "%"
            }

            ValueButton {
                id: backgroundColorValueButton

                property var colors: ["transparent", "#FFFFFF", "#E4E4DA", "#F5F3D7", "#D3F2EA", "#87C0EA", "#F5C189", "#E2F99A", "#FFFF99", "#D9BB7B", "#68C3E2", "#9C9291", "#CDA4DE", "#EE9DC3", "#FFFF00", "#469BC3", "#F49B00", "#95B90B", "#00CC00", "#AA7D55", "#059D9E", "#478CC6", "#D67240", "#8D7452", "#FF6600", "#5F8265", "#5E748C", "#A06EB9", "#009900", "#DE378B", "#FF0000", "#A83D15", "#4C5156", "#990066", "#0000FF", "#80081B", "#5B1C0C", "#002541", "#003300", "#2C1577", "#300F06", "#000000"]

                label: qsTr("Background color")
                value: chosenColor.length === 0 ? qsTr("Select") : (Qt.colorEqual(chosenColor, "transparent") ? qsTr("Transparent") : "")

                onClicked: {
                    var page = pageStack.push("Sailfish.Silica.ColorPickerPage", {
                            "colors": colors
                        });
                    page.colorClicked.connect(function (color) {
                            chosenColor = color;
                            pageStack.pop();
                        });
                }

                Rectangle {
                    anchors {
                        right: parent.right
                        rightMargin: Theme.horizontalPageMargin
                    }
                    width: height
                    height: Theme.itemSizeSmall
                    color: chosenColor
                    radius: 5
                    visible: chosenColor.length !== 0 && !Qt.colorEqual(chosenColor, "transparent")
                }
            }

            ValueButton {
                id: pathValueButton

                label: qsTr("Save to")
                value: chosenFolderPath.replace("file://", "").replace(StandardPaths.home + "/", "")

                onClicked: {
                    var dialog = pageStack.push(Qt.resolvedUrl("../dialogs/SelectFolderDialog.qml"), {
                            "currentFolder": chosenFolderPath
                        });
                    dialog.accepted.connect(function () {
                            chosenFolderPath = dialog.currentFolder;
                        });
                }
            }
        }
    }
}
