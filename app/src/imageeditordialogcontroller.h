// SPDX-FileCopyrightText: 2023 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause


#ifndef IMAGEEDITORDIALOGCONTROLLER_H
#define IMAGEEDITORDIALOGCONTROLLER_H

#include <QObject>

#include "settings.h"

/*!
 * \brief ImageEditorDialogController class is a controller for ImageEditorDialog.
 */
class ImageEditorDialogController : public QObject
{
    Q_OBJECT
    Q_PROPERTY(Settings *settings READ settings WRITE setSettings NOTIFY settingsChanged)
    Q_PROPERTY(double brightness READ brightness WRITE setBrightness NOTIFY brightnessChanged)
    Q_PROPERTY(double contrast READ contrast WRITE setContrast NOTIFY contrastChanged)
    Q_PROPERTY(int rotation READ rotation WRITE addRotation NOTIFY rotationChanged)
    Q_PROPERTY(bool brightnessEdited READ brightnessEdited NOTIFY brightnessEditedChanged)
    Q_PROPERTY(bool contrastEdited READ contrastEdited NOTIFY contrastEditedChanged)
    Q_PROPERTY(bool rotationEdited READ rotationEdited NOTIFY rotationEditedChanged)

public:
    explicit ImageEditorDialogController(QObject *parent = nullptr);

    // Getters
    Settings *settings();
    double brightness() const;
    double contrast() const;
    int rotation() const;
    bool brightnessEdited() const;
    bool contrastEdited() const;
    bool rotationEdited() const;

    // Setters
    void setSettings(Settings *settings);
    void setBrightness(const double brightness);
    void setContrast(const double contrast);

    Q_INVOKABLE void resetBrightness();
    Q_INVOKABLE void resetContrast();
    Q_INVOKABLE double defaultBrightness() const;
    Q_INVOKABLE double defaultContrast() const;

    Q_INVOKABLE void deleteFile(const QString &filePath);

public slots:
    void addRotation(const int rotation);

private:
    // Setters
    void setBrightnessEdited(const bool brightnessEdited);
    void setContrastEdited(const bool contrastEdited);
    void setRotationEdited(const bool rotationEdited);

private:
    static const double s_defaultBrightness;
    static const double s_defaultContrast;

    Settings *m_settings;
    double m_brightness;
    double m_contrast;
    int m_rotation;
    bool m_brightnessEdited;
    bool m_contrastEdited;
    bool m_rotationEdited;

signals:
    void settingsChanged();
    void brightnessChanged();
    void contrastChanged();
    void rotationChanged();
    void brightnessEditedChanged();
    void contrastEditedChanged();
    void rotationEditedChanged();
};

#endif // IMAGEEDITORDIALOGCONTROLLER_H
