// SPDX-FileCopyrightText: 2023 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

#include "pensettingscontroller.h"

PenSettingsController::PenSettingsController(QObject *parent)
    : QObject(parent), m_settings(nullptr), m_currentColor("EE4242"), m_penSize(5)
{
    QStringList colors = { "#000000", "#5D5D5D", "#B5B5B5", "#FFFFFF", "#EE4242", "#EBEE42",
                           "#7BDB00", "#026E00", "#2CB3FF", "#0316C0", "#A12AFF", "#FB2AFF" };
    for (QString color : colors) {
        QVariantMap colorMap;
        colorMap[QStringLiteral("color")] = QVariant(color);
        colorMap[QStringLiteral("checked")] = QVariant(false);
        m_colors.append(colorMap);
    }
    emit colorsChanged();
}

Settings *PenSettingsController::settings()
{
    return m_settings;
}

QVariantList PenSettingsController::colors() const
{
    return m_colors;
}

QColor PenSettingsController::currentColor() const
{
    return m_currentColor;
}

void PenSettingsController::setCurrentColor(const QColor color)
{
    m_currentColor = color;
    emit currentColorChanged();
    updateColors(color);
}

void PenSettingsController::updateColors(const QColor color)
{
    for (QVariant &colorVariant : m_colors) {
        QVariantMap colorMap;
        colorMap[QStringLiteral("color")] = QVariant(colorVariant.toMap()["color"]);
        if (QColor(colorVariant.toMap()["color"].toString()) == color)
            colorMap[QStringLiteral("checked")] = QVariant(true);
        else
            colorMap[QStringLiteral("checked")] = QVariant(false);
        colorVariant = colorMap;
    }
    emit colorsChanged();
}

int PenSettingsController::penSize()
{
    return m_penSize;
}

void PenSettingsController::setSettings(Settings *settings)
{
    m_settings = settings;
    m_penSize = m_settings->penSize();
    m_currentColor = m_settings->penColor();
    emit settingsChanged();
}

void PenSettingsController::setPenSize(int size)
{
    m_penSize = size;
    emit penSizeChanged();
}

/*!
 * \brief Saves the chosen pen size and color into settings.
 */
void PenSettingsController::submitPenSizeAndColor()
{
    m_settings->setPenSize(m_penSize);
    m_settings->setPenColor(m_currentColor);
}
