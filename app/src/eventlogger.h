// SPDX-FileCopyrightText: 2023 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

#ifndef EVENTLOGGER_H
#define EVENTLOGGER_H

#include <QDebug>
#include <QFile>
#include <stdio.h>
#include <QDate>
#include <QDir>
#include <QIODevice>
#include <QStandardPaths>
#include <QTextCodec>

/*!
 * \brief Class which handles console messages
 */
class EventLogger
{
public:
    EventLogger(const QString dir = QDir::homePath() + DOCUMENTS_LOCATION);
    ~EventLogger();

    void handleMessage(QtMsgType msgType, const QMessageLogContext &context, const QString &msg);
    static QString variantMapToJsonString(const QVariantMap &map);

    void setShowLogs(const bool showLogs);

private:
    void cleanup();
    void prepareOutputStream();
    bool prepareWritableFile();
    void changeLogFileIfNeeded();

    bool m_showLogs;
    QFile debugFile;
    QTextStream debugStream;
    QString directory;
    QString fileName;
    QHash<int, QString> messageTypes{ { QtDebugMsg, "Debug" },
                                      { QtWarningMsg, "Warning" },
                                      { QtCriticalMsg, "Critical" },
                                      { QtFatalMsg, "Fatal" },
                                      { QtInfoMsg, "Info" } };
    QDate m_logFileDate;
};

#endif // EVENTLOGGER_H
