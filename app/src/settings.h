// SPDX-FileCopyrightText: 2023 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

#ifndef SETTINGS_H
#define SETTINGS_H

#include <QObject>
#include <QStandardPaths>
#include <QSettings>
#include <QColor>

class Settings : public QObject
{
    Q_OBJECT
    Q_PROPERTY(int penSize READ penSize WRITE setPenSize NOTIFY penSizeChanged)
    Q_PROPERTY(QColor penColor READ penColor WRITE setPenColor NOTIFY penColorChanged)
    Q_PROPERTY(int cropFormat READ cropFormat WRITE setCropFormat NOTIFY cropFormatChanged)
    Q_PROPERTY(QString cropFormatName READ cropFormatName NOTIFY cropFormatNameChanged)

public:
    enum CropFormat { Original = 0, Custom = 1, Square = 2, FourXThree = 3, ThreeXFour = 4 };
    Q_ENUM(CropFormat);

    explicit Settings(const QString &confFilePath =
                              QStandardPaths::writableLocation(QStandardPaths::AppConfigLocation)
                                      .append("/appConfig.ini"),
                      QObject *parent = nullptr);

    // Setters
    void setPenSize(const int penSize);
    void setPenColor(const QColor &penColor);
    void setCropFormat(const int cropFormat);

    // Getters
    int penSize() const;
    QColor penColor() const;
    int cropFormat() const;
    QString cropFormatName() const;

private:
    static const QString s_penSizeKey;
    static const QString s_penColorKey;
    static const QString s_cropFormatKey;

private:
    void setValue(const QString &key, const QVariant &value = QVariant());
    QVariant value(const QString &key, const QVariant &defaultValue = QVariant()) const;

private:
    QSettings *m_settings;
    QString m_cropFormatString;

signals:
    void penSizeChanged();
    void penColorChanged();
    void cropFormatChanged(const int newCropFormat);
    void cropFormatNameChanged();
};

#endif // SETTINGS_H
