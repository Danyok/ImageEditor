// SPDX-FileCopyrightText: 2023 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

#include <QDebug>
#include "settings.h"

// Settings keys
const QString Settings::s_penSizeKey = "pen_size";
const QString Settings::s_penColorKey = "pen_color";
const QString Settings::s_cropFormatKey = "crop_format";

Settings::Settings(const QString &confFilePath, QObject *parent) : QObject(parent)
{
    qInfo() << "Create settings" << confFilePath;
    if (confFilePath.isEmpty()) {
        m_settings = new QSettings(this);
    } else {
        m_settings = new QSettings(confFilePath, QSettings::IniFormat, this);
    }
}

/*!
 * \brief Saves the pen size into settings.
 * \param penSize Pen size to save.
 */
void Settings::setPenSize(const int penSize)
{
    setValue(s_penSizeKey, penSize);
    emit penSizeChanged();
}

/*!
 * \brief Saves the pen color value into settings.
 * \param penColor Pen color to save.
 */
void Settings::setPenColor(const QColor &penColor)
{
    setValue(s_penColorKey, penColor);
    emit penColorChanged();
}

/*!
 * \brief Saves the crop format value into settings.
 * \param cropFormat CropFormat enum value.
 */
void Settings::setCropFormat(const int cropFormat)
{
    setValue(s_cropFormatKey, cropFormat);
    emit cropFormatChanged(cropFormat);
    emit cropFormatNameChanged();
}

/*!
 * \return Saved pen size value or 5 as default.
 */
int Settings::penSize() const
{
    return value(s_penSizeKey, 5).toInt();
}

/*!
 * \return Saved pen color or QColor("#EE4242") as default.
 */
QColor Settings::penColor() const
{
    return value(s_penColorKey, QColor("#EE4242")).value<QColor>();
}

/*!
 * \return Saved crop format value or CropFormat::Original as default.
 */
int Settings::cropFormat() const
{
    return value(s_cropFormatKey, CropFormat::Original).toInt();
}

/*!
 * \brief Retrieves the string representation of the saved crop format.
 * \return QString with the crop format name.
 */
QString Settings::cropFormatName() const
{
    switch (cropFormat()) {
    case CropFormat::Original:
        return QObject::tr("Original");
    case CropFormat::Custom:
        return QObject::tr("Custom");
    case CropFormat::Square:
        return QObject::tr("Square");
    case CropFormat::FourXThree:
        return QStringLiteral("4:3");
    case CropFormat::ThreeXFour:
        return QStringLiteral("3:4");
    default:
        return QStringLiteral("Unknown");
    }
}

/*!
 * \brief Saves the given value with into settings by the given key.
 * \param key Setting key.
 * \param value Value to save.
 */
void Settings::setValue(const QString &key, const QVariant &value)
{
    m_settings->setValue(key, value);
    m_settings->sync();
}

/*!
 * \brief Retrieves the setting value by the given key.
 * \param key Setting key.
 * \param defaultValue Default value of the setting is empty.
 * \return Setting value as QVariant.
 */
QVariant Settings::value(const QString &key, const QVariant &defaultValue) const
{
    m_settings->sync();
    return m_settings->value(key, defaultValue);
}
