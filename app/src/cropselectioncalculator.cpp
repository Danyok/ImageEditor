// SPDX-FileCopyrightText: 2023 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

#include <QSize>
#include <QLineF>

#include "cropselectioncalculator.h"

const int CropSelectionCalculator::s_cropTouchRadius = 100;
const qreal CropSelectionCalculator::s_fourAtThree = 4.0 / 3;
const qreal CropSelectionCalculator::s_threeAtFour = 3.0 / 4;

/*!
 * \brief Constructor initializes the class fields.
 * \param parent QObject parent instance.
 */
CropSelectionCalculator::CropSelectionCalculator(QObject *parent)
    : QObject(parent), m_cropFormat(Settings::CropFormat::Original)
{
}

int CropSelectionCalculator::cropFormat() const
{
    return m_cropFormat;
}

void CropSelectionCalculator::setCropFormat(const int cropFormat)
{
    m_cropFormat = cropFormat;
    emit cropFormatChanged();
}

/*!
 * \brief Creates a new selection recangle by he given arguments.
 * \param rectTopLeft Point at top left corner of the selection rectangle.
 * \param rectBottomRight Point at bottom right corner of the selection rectangle.
 * \return New selection rectangle created by the given ponts.
 */
QRectF CropSelectionCalculator::createCropAreaRect(QPointF rectTopLeft,
                                                   QPointF rectBottomRight) const
{
    qreal width = rectBottomRight.x() - rectTopLeft.x();
    qreal height = rectBottomRight.y() - rectTopLeft.y();
    if (m_cropFormat == Settings::CropFormat::Square) {
        qreal size = width < height ? width : height;
        return QRectF(rectTopLeft, QSize(size, size));
    } else if (m_cropFormat == Settings::CropFormat::FourXThree) {
        if (width > s_fourAtThree * height) {
            return QRectF(rectTopLeft, QSize(s_fourAtThree * height, height));
        } else {
            return QRectF(rectTopLeft, QSize(width, width / s_fourAtThree));
        }
    } else if (m_cropFormat == Settings::CropFormat::ThreeXFour) {
        if (width > s_threeAtFour * height) {
            return QRectF(rectTopLeft, QSize(s_threeAtFour * height, height));
        } else {
            return QRectF(rectTopLeft, QSize(width, width / s_threeAtFour));
        }
    } else {
        return QRectF(rectTopLeft, rectBottomRight);
    }
}

/*!
 * \brief Creates a new crop area recangle with the current crop format.
 * \param origRect Original crop area rectangle to create a new one.
 * \return New crop area rectangle.
 */
QRectF CropSelectionCalculator::createCropAreaRect(QRectF origRect)
{
    return createCropAreaRect(origRect.topLeft(), origRect.bottomRight());
}

/*!
 * \brief Calculates the new selection rectangle in depend on the original one and current position
 * of the user touch for the current crop format.
 * \param currentPosition Current possition of user touch.
 * \param origRect Original selection rectangle.
 * \param targetRect Target image rectangle to bound.
 * \return Calculated new selection rectangle for the current crop format.
 */
QRectF CropSelectionCalculator::calculateNewCropAreaRect(QPointF currentPosition, QRectF origRect,
                                                         QRectF targetRect)
{
    switch (m_cropFormat) {
    case Settings::CropFormat::Custom:
        return calculateCustomSelectionRect(currentPosition, origRect.topLeft(),
                                            origRect.bottomRight(), targetRect);
    case Settings::CropFormat::Square:
        return calculateSquareSelectionRect(currentPosition, origRect.topLeft(),
                                            origRect.bottomRight(), targetRect);
    case Settings::CropFormat::FourXThree:
        return calculateFourXThreeSelectionRect(currentPosition, origRect.topLeft(),
                                                origRect.bottomRight(), targetRect);
    case Settings::CropFormat::ThreeXFour:
        return calculateThreeXFourSelectionRect(currentPosition, origRect.topLeft(),
                                                origRect.bottomRight(), targetRect);
    default:
        return calculateOriginalSelectionRect(currentPosition, origRect.topLeft(),
                                              origRect.bottomRight(), targetRect);
    }
}

/*!
 * \brief Calculates the new selection rectangle in depend on the original one and current position
 * of the user touch for the 'Original' crop format.
 * \param currentPosition Current possition of user touch.
 * \param origRectTopLeft Original selection rectangle top left corner.
 * \param origRectBottomRight Original selection rectangle bottom right corner.
 * \param targetRect Target image rectangle to bound.
 * \return Calculated new selection rectangle for the 'Original' crop format.
 */
QRectF CropSelectionCalculator::calculateOriginalSelectionRect(QPointF currentPosition,
                                                               QPointF origRectTopLeft,
                                                               QPointF origRectBottomRight,
                                                               QRectF targetRect) const
{
    QRectF origSelectionRect(origRectTopLeft, origRectBottomRight);
    qreal ratio = origSelectionRect.width() / origSelectionRect.height();
    return calculateSelectionRectWithRatio(currentPosition, origRectTopLeft, origRectBottomRight,
                                           ratio, targetRect);
}

/*!
 * \brief Calculates the new selection rectangle in depend on the original one and current position
 * of the user touch for the 'Custom' crop format.
 * \param currentPosition Current possition of user touch.
 * \param origRectTopLeft Original selection rectangle top left corner.
 * \param origRectBottomRight Original selection rectangle bottom right corner.
 * \param targetRect Target image rectangle to bound.
 * \return Calculated new selection rectangle for the 'Custom' crop format.
 */
QRectF CropSelectionCalculator::calculateCustomSelectionRect(QPointF currentPosition,
                                                             QPointF origRectTopLeft,
                                                             QPointF origRectBottomRight,
                                                             QRectF targetRect) const
{
    QRectF newSelectionRect = createCropAreaRect(origRectTopLeft, origRectBottomRight);
    if (isCroppingFromTopLeft(currentPosition, newSelectionRect)) {
        newSelectionRect.setTopLeft(currentPosition);
    } else if (isCroppingFromTopRight(currentPosition, newSelectionRect)) {
        newSelectionRect.setTopRight(currentPosition);
    } else if (isCroppingFromBottomLeft(currentPosition, newSelectionRect)) {
        newSelectionRect.setBottomLeft(currentPosition);
    } else if (isCroppingFromBottomRight(currentPosition, newSelectionRect)) {
        newSelectionRect.setBottomRight(currentPosition);
    }
    return boundNewSelectionRect(currentPosition, newSelectionRect, targetRect);
}

/*!
 * \brief Calculates the new selection rectangle in depend on the original one and current position
 * of the user touch for the 'Square' crop format.
 * \param currentPosition Current possition of user touch.
 * \param origRectTopLeft Original selection rectangle top left corner.
 * \param origRectBottomRight Original selection rectangle bottom right corner.
 * \param targetRect Target image rectangle to bound.
 * \return Calculated new selection rectangle for the 'Square' crop format.
 */
QRectF CropSelectionCalculator::calculateSquareSelectionRect(QPointF currentPosition,
                                                             QPointF origRectTopLeft,
                                                             QPointF origRectBottomRight,
                                                             QRectF targetRect) const
{
    return calculateSelectionRectWithRatio(currentPosition, origRectTopLeft, origRectBottomRight,
                                           1.0, targetRect);
}

/*!
 * \brief Calculates the new selection rectangle in depend on the original one and current position
 * of the user touch for the '4:3' crop format.
 * \param currentPosition Current possition of user touch.
 * \param origRectTopLeft Original selection rectangle top left corner.
 * \param origRectBottomRight Original selection rectangle bottom right corner.
 * \param targetRect Target image rectangle to bound.
 * \return Calculated new selection rectangle for the '4:3' crop format.
 */
QRectF CropSelectionCalculator::calculateFourXThreeSelectionRect(QPointF currentPosition,
                                                                 QPointF origRectTopLeft,
                                                                 QPointF origRectBottomRight,
                                                                 QRectF targetRect) const
{
    return calculateSelectionRectWithRatio(currentPosition, origRectTopLeft, origRectBottomRight,
                                           s_fourAtThree, targetRect);
}

/*!
 * \brief Calculates the new selection rectangle in depend on the original one and current position
 * of the user touch for the '3:4' crop format.
 * \param currentPosition Current possition of user touch.
 * \param origRectTopLeft Original selection rectangle top left corner.
 * \param origRectBottomRight Original selection rectangle bottom right corner.
 * \param targetRect Target image rectangle to bound.
 * \return Calculated new selection rectangle for the '3:4' crop format.
 */
QRectF CropSelectionCalculator::calculateThreeXFourSelectionRect(QPointF currentPosition,
                                                                 QPointF origRectTopLeft,
                                                                 QPointF origRectBottomRight,
                                                                 QRectF targetRect) const
{
    return calculateSelectionRectWithRatio(currentPosition, origRectTopLeft, origRectBottomRight,
                                           s_threeAtFour, targetRect);
}

/*!
 * \brief Calculates the new selection rectangle in depend on the original one, current position
 * of the user touch and the cop ratio for the 'Square' crop format.
 * \param currentPosition Current possition of user touch.
 * \param origRectTopLeft Original selection rectangle top left corner.
 * \param origRectBottomRight Original selection rectangle bottom right corner.
 * \param ratio Crop ratio.
 * \param targetRect Target image rectangle to bound.
 * \return Calculated new selection rectangle for the 'Square' crop format.
 */
QRectF CropSelectionCalculator::calculateSelectionRectWithRatio(QPointF currentPosition,
                                                                QPointF origRectTopLeft,
                                                                QPointF origRectBottomRight,
                                                                const qreal ratio,
                                                                QRectF targetRect) const
{
    QRectF origSelectionRect(origRectTopLeft, origRectBottomRight);
    QRectF newSelectionRect = createCropAreaRect(origRectTopLeft, origRectBottomRight);
    if (isCroppingFromTopLeft(currentPosition, newSelectionRect)) {
        qreal newY = newSelectionRect.topLeft().y()
                + (currentPosition.x() - newSelectionRect.topLeft().x()) / ratio;
        if (currentPosition.x() > targetRect.topLeft().x() && newY > targetRect.topLeft().y())
            newSelectionRect.setTopLeft(QPointF(currentPosition.x(), newY));
        newSelectionRect.setBottomRight(origSelectionRect.bottomRight());
    } else if (isCroppingFromTopRight(currentPosition, newSelectionRect)) {
        qreal newY = newSelectionRect.topRight().y()
                + (newSelectionRect.topRight().x() - currentPosition.x()) / ratio;
        if (currentPosition.x() < targetRect.topRight().x() && newY > targetRect.topRight().y())
            newSelectionRect.setTopRight(QPointF(currentPosition.x(), newY));
        newSelectionRect.setBottomLeft(origSelectionRect.bottomLeft());
    } else if (isCroppingFromBottomLeft(currentPosition, newSelectionRect)) {
        qreal newY = newSelectionRect.bottomLeft().y()
                - (currentPosition.x() - newSelectionRect.bottomLeft().x()) / ratio;
        if (currentPosition.x() > targetRect.bottomLeft().x() && newY < targetRect.bottomLeft().y())
            newSelectionRect.setBottomLeft(QPointF(currentPosition.x(), newY));
        newSelectionRect.setTopRight(origSelectionRect.topRight());
    } else if (isCroppingFromBottomRight(currentPosition, newSelectionRect)) {
        qreal newY = newSelectionRect.bottomRight().y()
                - (newSelectionRect.bottomRight().x() - currentPosition.x()) / ratio;
        if (currentPosition.x() < targetRect.bottomRight().x()
            && newY < targetRect.bottomRight().y())
            newSelectionRect.setBottomRight(QPointF(currentPosition.x(), newY));
        newSelectionRect.setTopLeft(origSelectionRect.topLeft());
    }
    return newSelectionRect;
}

/*!
 * \brief Bounds the new selection rectangle in depend on the given target one.
 * This medthod is needed to bound the new crop area rectangle inside the target image sizes.
 * \param currentPosition Current touch position to determine crop area corner.
 * \param newSelectionRect New calculated selection rectangle.
 * \param targetRect target rectangle to bound the new one.
 * \return Bounded new selection rectangle.
 */
QRectF CropSelectionCalculator::boundNewSelectionRect(QPointF currentPosition,
                                                      QRectF newSelectionRect,
                                                      QRectF targetRect) const
{
    if (isCroppingFromTopLeft(currentPosition, newSelectionRect)) {
        QPointF point = newSelectionRect.topLeft();
        if (newSelectionRect.topLeft().x() < targetRect.topLeft().x())
            point.setX(targetRect.topLeft().x());
        if (newSelectionRect.topLeft().y() < targetRect.topLeft().y())
            point.setY(targetRect.topLeft().y());
        newSelectionRect.setTopLeft(point);
    } else if (isCroppingFromTopRight(currentPosition, newSelectionRect)) {
        QPointF point = newSelectionRect.topRight();
        if (newSelectionRect.topRight().x() > targetRect.topRight().x())
            point.setX(targetRect.topRight().x());
        if (newSelectionRect.topRight().y() < targetRect.topRight().y())
            point.setY(targetRect.topRight().y());
        newSelectionRect.setTopRight(point);
    } else if (isCroppingFromBottomLeft(currentPosition, newSelectionRect)) {
        QPointF point = newSelectionRect.bottomLeft();
        if (newSelectionRect.bottomLeft().x() < targetRect.bottomLeft().x())
            point.setX(targetRect.bottomLeft().x());
        if (newSelectionRect.bottomLeft().y() > targetRect.bottomLeft().y())
            point.setY(targetRect.bottomLeft().y());
        newSelectionRect.setBottomLeft(point);
    } else if (isCroppingFromBottomRight(currentPosition, newSelectionRect)) {
        QPointF point = newSelectionRect.bottomRight();
        if (newSelectionRect.bottomRight().x() > targetRect.bottomRight().x())
            point.setX(targetRect.bottomRight().x());
        if (newSelectionRect.bottomRight().y() > targetRect.bottomRight().y())
            point.setY(targetRect.bottomRight().y());
        newSelectionRect.setBottomRight(point);
    }
    return newSelectionRect;
}

/*!
 * \brief Determines whether the current touch position is around the selection rectangle top
 * left corner.
 * \param curPos Current touch position.
 * \param selectRect Selection rectangle.
 * \return True if current position is around the top left corner.
 */
bool CropSelectionCalculator::isCroppingFromTopLeft(QPointF curPos, QRectF selectRect) const
{
    return QLineF(curPos, selectRect.topLeft()).length() < s_cropTouchRadius;
}

/*!
 * \brief Determines whether the current touch position is around the selection rectangle top
 * right corner.
 * \param curPos Current touch position.
 * \param selectRect Selection rectangle.
 * \return True if current position is around the top right corner.
 */
bool CropSelectionCalculator::isCroppingFromTopRight(QPointF curPos, QRectF selectRect) const
{
    return QLineF(curPos, selectRect.topRight()).length() < s_cropTouchRadius;
}

/*!
 * \brief Determines whether the current touch position is around the selection rectangle bottom
 * left corner.
 * \param curPos Current touch position.
 * \param selectRect Selection rectangle.
 * \return True if current position is around the bottom left corner.
 */
bool CropSelectionCalculator::isCroppingFromBottomLeft(QPointF curPos, QRectF selectRect) const
{
    return QLineF(curPos, selectRect.bottomLeft()).length() < s_cropTouchRadius;
}

/*!
 * \brief Determines whether the current touch position is around the selection rectangle bottom
 * right corner.
 * \param curPos Current touch position.
 * \param selectRect Selection rectangle.
 * \return True if current position is around the bottom right corner.
 */
bool CropSelectionCalculator::isCroppingFromBottomRight(QPointF curPos, QRectF selectRect) const
{
    return QLineF(curPos, selectRect.bottomRight()).length() < s_cropTouchRadius;
}
