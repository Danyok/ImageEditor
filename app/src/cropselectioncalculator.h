// SPDX-FileCopyrightText: 2023 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause


#ifndef CROPSELECTIONCALCULATOR_H
#define CROPSELECTIONCALCULATOR_H

#include <QObject>
#include <QRectF>

#include "settings.h"

/*!
 * \brief CropSelectionCalculator class provides methods to calculate selection rectangles
 * in depend on the crop format.
 */
class CropSelectionCalculator : public QObject
{
    Q_OBJECT
    Q_PROPERTY(int cropFormat READ cropFormat WRITE setCropFormat NOTIFY cropFormatChanged)

public:
    explicit CropSelectionCalculator(QObject *parent = nullptr);

    // Getters
    int cropFormat() const;

    // Setters
    void setCropFormat(const int cropFormat);

    Q_INVOKABLE QRectF createCropAreaRect(QRectF origRect);
    Q_INVOKABLE QRectF calculateNewCropAreaRect(QPointF currentPosition, QRectF origRect,
                                                QRectF targetRect);

private:
    QRectF createCropAreaRect(QPointF rectTopLeft, QPointF rectBottomRight) const;
    QRectF calculateOriginalSelectionRect(QPointF currentPosition, QPointF origRectTopLeft,
                                          QPointF origRectBottomRight, QRectF targetRect) const;
    QRectF calculateCustomSelectionRect(QPointF currentPosition, QPointF origRectTopLeft,
                                        QPointF origRectBottomRight, QRectF targetRect) const;
    QRectF calculateSquareSelectionRect(QPointF currentPosition, QPointF origRectTopLeft,
                                        QPointF origRectBottomRight, QRectF targetRect) const;
    QRectF calculateFourXThreeSelectionRect(QPointF currentPosition, QPointF origRectTopLeft,
                                            QPointF origRectBottomRight, QRectF targetRect) const;
    QRectF calculateThreeXFourSelectionRect(QPointF currentPosition, QPointF origRectTopLeft,
                                            QPointF origRectBottomRight, QRectF targetRect) const;
    QRectF calculateSelectionRectWithRatio(QPointF currentPosition, QPointF origRectTopLeft,
                                           QPointF origRectBottomRight, const qreal ratio, QRectF targetRect) const;
    QRectF boundNewSelectionRect(QPointF currentPosition, QRectF newSelectionRect,
                                 QRectF targetRect) const;

    bool isCroppingFromTopLeft(QPointF curPos, QRectF selectRect) const;
    bool isCroppingFromTopRight(QPointF curPos, QRectF selectRect) const;
    bool isCroppingFromBottomLeft(QPointF curPos, QRectF selectRect) const;
    bool isCroppingFromBottomRight(QPointF curPos, QRectF selectRect) const;

private:
    static const int s_cropTouchRadius;
    static const qreal s_fourAtThree;
    static const qreal s_threeAtFour;

    int m_cropFormat;

signals:
    void cropFormatChanged();
};

#endif // CROPSELECTIONCALCULATOR_H
