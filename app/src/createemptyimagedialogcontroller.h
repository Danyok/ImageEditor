// SPDX-FileCopyrightText: 2023 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

#ifndef CREATEEMPTYIMAGEDIALOGCONTROLLER_H
#define CREATEEMPTYIMAGEDIALOGCONTROLLER_H

#include <QObject>
#include <QColor>

/*!
 * \brief CreateEmptyImageDialogController class is a controller for the "Create Empty Image"
 * dialog.
 * The class allows to create and save a new empty image.
 */
class CreateEmptyImageDialogController : public QObject
{
    Q_OBJECT

public:
    explicit CreateEmptyImageDialogController(QObject *parent = nullptr);

    Q_INVOKABLE QString createAndSaveImage(const int width, const int height,
                                           const QString &imageFormat,
                                           const QColor &backgroundColor, const int quality,
                                           QString saveToPath);
};

#endif // CREATEEMPTYIMAGEDIALOGCONTROLLER_H
